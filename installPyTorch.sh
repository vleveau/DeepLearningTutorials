export NO_CUDA=1
export CMAKE_PREFIX_PATH=$1

conda install numpy pyyaml mkl setuptools cmake cffi
conda install -c soumith magma-cuda80

git clone --recursive https://github.com/pytorch/pytorch
cd pytorch
python setup.py install

git clone https://github.com/pytorch/vision.git
cd vision
python setup.py install

cd ../
git clone https://github.com/pytorch/examples.git
cd examples/mnist
python main.py

echo TOUT EST OK !!!
cd ../../../
